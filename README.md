# ASR-CHiME 5

## Goal:

Building an ASR system for the CHiME 5 challenge dataset.

### Receipe for JHU system for CHiME 5.

#### Improvements from the baseline system:


*  Simulation of reverberated speech using worn microphones.

*  Noise augmentation using noise extracted from the training data.

*  Weighted prediction error dereverberation before beamforming at test time.

#### How to run the recipe.
1. First pull this repo and copy s5_jhu_chime5/ in your kaldi setup at /opt/kaldi/egs/chime5/
2. Change "chime5_corpus" path in run.sh to the according location of the chime5 corpus.
3. You need to install ICU4C, srlim, sox, phonetisaurus, Beamformit and wpe in order to pass the check_tools step.
3. WARNING: When downloading srlim manually make sure that the compressed file is in .tar.gz format otherwise the opening will go wrong and it will crash when training the LMs.
4. You can find the instraction on how to install all this packages in "kaldi/egs/chime5/s5b/local/check_tools.sh"
5. Now run all the stages of run.sh. Stage 17 is the training of the chain model and executes the script "local/chain/tuning/run_tdnn_1b.sh"
6. In this scripts you can change the parameters "--trainer.optimization.num-jobs-initial" and "--trainer.optimization.num-jobs-final" to save some time but the defaults works fine.
7. Once the decoding is over run out_results.sh to ouput the results of the different models on the dev set.

#### Results:

We expect a WER < 73% for the best model.
 

