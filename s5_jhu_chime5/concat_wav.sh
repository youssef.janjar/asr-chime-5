#!/usr/bin/env bash


./utils/parse_options.sh


dset=$1

audio_dir="wav/wpe/$dset/split"

cd $audio_dir

for f in *001.wav
do
    pre=${f%%001.wav}
    # use either cat or sox or whatever works:
    #cat "${pre}_"*.wav > "${pre}.wav"
    echo "${pre}"*.wav
    sox "${pre}"*.wav "../${pre}.wav"
    #rm -rf "${pre}_"*.wav
done