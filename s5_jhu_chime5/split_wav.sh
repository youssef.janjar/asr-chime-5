#!/usr/bin/env bash


./utils/parse_options.sh

chime5_corpus=$1
dset=$2

cd "${chime5_corpus}/audio/${dset}"

echo "We are in: ${chime5_corpus}/audio/${dset}"
find . -name '*.wav' \
 -exec sh -c 'mkdir -p split/$(dirname "{}")' \; \
 -exec sox {} split/{}  trim 0 180 : newfile : restart \;